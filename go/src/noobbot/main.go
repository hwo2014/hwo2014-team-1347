package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net"
	"os"
	"runtime"
	"sync"
)

// Game conneciton variables
// var host = "testserver.helloworldopen.com"
// var port = 8091
// var name = "GoExhaust"
// var key = "Fudhe9U2+poUvA"

// Global variables
var logToFile = false
var gameInit GameInit                     // Game init message received by server
var howManyPieces = 0                     // Total number of pieces in track
var decelerationRef = map[int](float64){} // Deceleration in one tick based on initial speed
var lastCarPosition CarPosition           // Last CarPosition message received from server

// Magic variables
var defaultCurveEntrySpeedUnder100 = 3.0 // Since we don't know the track's friction value, set a moderate default curve entry speed (split this on radius)
var defaultCurveEntrySpeedUnder200 = 5.0 // Since we don't know the track's friction value, set a moderate default curve entry speed (split this on radius)
var defaultCurveEntrySpeedUnder300 = 7.5 // Since we don't know the track's friction value, set a moderate default curve entry speed (split this on radius)
var lastSpeed = 0.0                      // Last measured speed
var lastThrottle float32 = -1.0          // Last set throttle
var lastDeceleration = 0.0               // Last measured deceleration
var maxDraftUnder100 = 20.0              // For curve, the maximum draft angle
var maxDraftUnder200 = 30.0              // For curve, the maximum draft angle
var maxDraftUnder300 = 40.0              // For curve, the maximum draft angle
var maxExitDraft = 45.0                  // For curve, max exit from curve to straight piece angle
var maxDraftAccelerationUnder100 = 0.5   // For curve radius under 100, the maximum draft angle change
var maxDraftAccelerationUnder200 = 1.0   // For curve radius under 200, the maximum draft angle change
var maxDraftAccelerationUnder300 = 2.0   // For curve radius under 300, the maximum draft angle change
var stableDraft = 0.05                   // For curve, if draft change is under X it is considered stable
var inCurveMaxSlowDown = 4               // For curve, after each X curve slow down, press on gas a bit else draft won't really go lower
var inCurveMaxSlowDownCount = 0          // Keeps count of in curve consecutive slow downs
var defaultThrottle float32 = 0.5        // Default throttle set in case of code failure, should never be used
var pendingSwitchCommand = "None"        // If a switch lane command was sent to server and not actioned yet
var lastLane = -1                        // Last tick lane index
var turboAvailable = false               // Sometimes a turbo appears...
var gameStarted = false                  // Commands sent before gameStarted are ignored, so keep track of it
var aggressivity = 1.1                   // For qualifications, if in last lap curve was done without a crash, take it faster based on exit draft
var qualifications = false               // Is it qualifications or a race
var longuestStraightStartIndex = -1      // Best piece to start turbo
var minTicksInCurveBeforeAccelerate = 3  // Don't accelerate at beggining of a curve, wait a bit
var inCurveTicksCount = 0                // How many ticks inside the curve
var initDone = false                     // Game init processing completed
var currentCurvePieceEntrySpeed = 0.0    // Keeps track of a curve's entry speed
var maxEntryDraftChange = 0.5
var lastTriedStraighten = false
var lastTriedStraightenDraftChange = 0.0

func main() {
	host, port, name, key, err := parse_args()

	if err != nil {
		log.Fatalf("Could not parse args")
	}

	runtime.GOMAXPROCS(runtime.NumCPU())

	if logToFile {
		f, err := os.OpenFile("test.txt", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
		if err != nil {
			// log.Printf("Could not log to file %s:", err)
			return
		}
		defer f.Close()
		log.SetOutput(f)
	}

	// Connect to server
	// log.Printf("Connecting to host=%v, port=%v, bot name=%v, key=%v\n", host, port, name, key)
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		// log.Printf("Connection error: %s", err)
		os.Exit(1)
	}
	defer conn.Close()

	// Start bot
	err = bot_loop(conn, name, key)
	if err != nil {
		// log.Printf("Bot errored: %s", err)
		os.Exit(1)
	}
}

func dispatch_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	switch msgtype {
	case "join":
		// log.Printf("join: %v", data) // no data to extract
		// log.Printf("join")
		send_ping(writer)
	case "gameInit":
		// log.Printf("gameInit: %v", data)
		// send data to gameInit parser (non blocking)
		go parseGameInit(data)
		send_ping(writer)
	case "gameStart":
		// log.Printf("gameStart") // no data to extract
		// log.Printf("gameStart")
		gameStarted = true
		lastThrottle = 1.0
		send_throttle(writer, 1.0)
	case "crash":
		log.Printf("crash") // we could check the car's speed at time of crash and where to adjust
		send_ping(writer)
	case "gameEnd":
		// log.Printf("gameEnded: %v", data)

		// for _pid, piece := range gameInit.Race.Track.Pieces {
		// 	length := 0.0
		// 	if piece.Length != nil {
		// 		length = *piece.Length
		// 	}
		// 	radius := 0.0
		// 	if piece.Radius != nil {
		// 		radius = *piece.Radius
		// 	}
		// 	angle := 0.0
		// 	if piece.Angle != nil {
		// 		angle = *piece.Angle
		// 	}

		// 	// log.Printf("(%d) Length:%v Radius:%v, Angle:%v, %#v, %#v, %#v", pid, length, radius, angle, piece.MaxSpeed, piece.MaxDraft, piece.NextCurveIndex)
		// }

		send_ping(writer)
	case "carPositions":
		// log.Printf("carPositions: %v", data) // lots of data
		sendNextCommand(writer, data)
	case "yourCar":
		// log.Printf("yourCar: %v", data)  // no data to extract
		// log.Printf("yourCar")
		send_ping(writer)
	case "lapFinished":
		log.Printf("lapFinished: %v", data)
		send_ping(writer)
	case "finish":
		log.Printf("finish: %v", data) // no data to extract
		// log.Printf("finish")
		send_ping(writer)
	case "spawn":
		// log.Printf("spawn: %v", data)
		lastThrottle = 1.0
		send_throttle(writer, 1.0)
	case "error":
		// log.Printf(fmt.Sprintf("Msg error: %v", data))
		send_ping(writer)
	case "turboAvailable":
		// log.Printf("setting turbo")
		turboAvailable = true
	default:
		// log.Printf("Got msg type: %s", msgtype)
		send_ping(writer)
	}
	return
}

func parseGameInit(data interface{}) {
	var wg sync.WaitGroup
	defer func() {
		wg.Wait()
		initDone = true
	}()

	// Put in a struct (marshal then unmarshal, bad hack)
	jdata, err := json.Marshal(data)
	if err != nil {
		// log.Printf("Could not marshal gameInit data: %s", err)
		return
	}

	err = json.Unmarshal(jdata, &gameInit)
	if err != nil {
		// log.Printf("Could not unmarshal gameInit data: %s", err)
		return
	}

	// Do some analysis on the track
	howManyPieces = len(gameInit.Race.Track.Pieces)

	// Qualifications?
	if len(gameInit.Race.Cars) == 1 {
		qualifications = true
	}

	// Set next piece index
	for pid, _ := range gameInit.Race.Track.Pieces {
		if pid == howManyPieces-1 {
			gameInit.Race.Track.Pieces[pid].NextPieceIndex = 0
		} else {
			gameInit.Race.Track.Pieces[pid].NextPieceIndex = pid + 1
		}
	}

	// Calculate the length of each lane of each piece
	wg.Add(1)
	go func() {
		defer wg.Done()
		for pid, piece := range gameInit.Race.Track.Pieces {
			gameInit.Race.Track.Pieces[pid].LaneLengths = map[int]float64{}
			if piece.Length != nil { // Straight piece
				for lid, _ := range gameInit.Race.Track.Lanes {
					gameInit.Race.Track.Pieces[pid].LaneLengths[lid] = *piece.Length
				}
			} else { // Curve piece
				for lid, lane := range gameInit.Race.Track.Lanes {
					if piece.Angle != nil && *piece.Angle < 0 { // curves to the left, inside lane is to the left of center
						gameInit.Race.Track.Pieces[pid].LaneLengths[lid] = math.Abs((*piece.Angle / 180.0) * math.Pi * (*piece.Radius + lane.DistanceFromCenter))
					} else { // curves to the right, inside lane is to the right of center
						gameInit.Race.Track.Pieces[pid].LaneLengths[lid] = math.Abs((*piece.Angle / 180.0) * math.Pi * (*piece.Radius + (-1 * lane.DistanceFromCenter)))
					}
				}
			}
		}
	}()

	// Set init maxSpeed and maxDraft values
	wg.Add(1)
	go func() {
		defer wg.Done()
		for pid, _ := range gameInit.Race.Track.Pieces {
			// Initialize maxSpeed and maxDraft map
			gameInit.Race.Track.Pieces[pid].MaxSpeed = map[int]float64{}
			// gameInit.Race.Track.Pieces[pid].MaxDraft = map[int]float64{}
			for i := 0; i < len(gameInit.Race.Track.Lanes); i++ {
				if gameInit.Race.Track.Pieces[pid].Radius != nil {
					if *gameInit.Race.Track.Pieces[pid].Radius < 100 {
						gameInit.Race.Track.Pieces[pid].MaxSpeed[i] = defaultCurveEntrySpeedUnder100
					} else if *gameInit.Race.Track.Pieces[pid].Radius < 200 {
						gameInit.Race.Track.Pieces[pid].MaxSpeed[i] = defaultCurveEntrySpeedUnder200
					} else {
						gameInit.Race.Track.Pieces[pid].MaxSpeed[i] = defaultCurveEntrySpeedUnder300
					}
				} else {
					gameInit.Race.Track.Pieces[pid].MaxSpeed[i] = defaultCurveEntrySpeedUnder300
				}
			}
		}
	}()

	// Find the next curve piece index and distance to it for each piece
	wg.Add(1)
	go func() {
		defer wg.Done()
		for pid, piece := range gameInit.Race.Track.Pieces {
			tmpNextCurveIndex := piece.NextPieceIndex
			tmpDistanceToNextCurve := 0.0
		A:
			for i := 0; i < howManyPieces; i++ {
				if gameInit.Race.Track.Pieces[tmpNextCurveIndex].Length == nil {
					break A
				}
				tmpDistanceToNextCurve += *gameInit.Race.Track.Pieces[tmpNextCurveIndex].Length
				tmpNextCurveIndex = gameInit.Race.Track.Pieces[tmpNextCurveIndex].NextPieceIndex
			}
			gameInit.Race.Track.Pieces[pid].DistanceToNextCurve = tmpDistanceToNextCurve
			gameInit.Race.Track.Pieces[pid].NextCurveIndex = tmpNextCurveIndex
		}
	}()

	// Find the next piece index for a switch lane
	wg.Add(1)
	go func() {
		defer wg.Done()
		for pid, piece := range gameInit.Race.Track.Pieces {
			tmpNextSwitchPieceIndex := piece.NextPieceIndex
		B:
			for i := 0; i < howManyPieces; i++ {
				if gameInit.Race.Track.Pieces[tmpNextSwitchPieceIndex].Switch != nil {
					break B
				}
				tmpNextSwitchPieceIndex = gameInit.Race.Track.Pieces[tmpNextSwitchPieceIndex].NextPieceIndex
			}
			gameInit.Race.Track.Pieces[pid].NextSwitchIndex = tmpNextSwitchPieceIndex

		}
	}()

	// Find the longuest straight lenght of last lap for turbo activation (qualifications only)
	wg.Add(1)
	go func() {
		defer wg.Done()
		longuestStraight := 0.0
		for pid, piece := range gameInit.Race.Track.Pieces {
			// Is it a straight piece
			if piece.Length != nil {
				length := *piece.Length
			T:
				for i := pid; i < howManyPieces; i++ {
					if gameInit.Race.Track.Pieces[i].Length != nil {
						length += *gameInit.Race.Track.Pieces[i].Length
					} else {
						break T
					}
				}
				if length > longuestStraight {
					longuestStraightStartIndex = pid
					longuestStraight = length
				}
			}
		}
	}()

	// Initialize deceleration reference map
	decelerationRef[0] = 0.1
	decelerationRef[1] = 0.2
	decelerationRef[2] = 0.4
	decelerationRef[3] = 0.6
	decelerationRef[4] = 0.8
	decelerationRef[5] = 0.10
	decelerationRef[6] = 0.11
	decelerationRef[7] = 0.13
	decelerationRef[8] = 0.15
	decelerationRef[9] = 0.17
	decelerationRef[10] = 0.19
	decelerationRef[11] = 0.21
	decelerationRef[12] = 0.23
	decelerationRef[13] = 0.25
	decelerationRef[14] = 0.27
	decelerationRef[15] = 0.29
	decelerationRef[16] = 0.30
	decelerationRef[17] = 0.32
	decelerationRef[18] = 0.34
	decelerationRef[19] = 0.36
	decelerationRef[20] = 0.38
	decelerationRef[21] = 0.40
	decelerationRef[22] = 0.42
	decelerationRef[23] = 0.44
	decelerationRef[24] = 0.46
	decelerationRef[25] = 0.48
	decelerationRef[26] = 0.50
	decelerationRef[27] = 0.52
	decelerationRef[28] = 0.54
	decelerationRef[29] = 0.56
	decelerationRef[30] = 0.58
}

func sendNextCommand(writer *bufio.Writer, data interface{}) (newThrottle float32, sideSwitch string, useTurbo bool) {
	speed := 0.0
	defer func() {
		if speed == 0.0 { // Not moving, accelerate!
			// log.Printf("Full blast")
			newThrottle = 1.0
			lastThrottle = newThrottle
			send_throttle(writer, newThrottle)
		} else if useTurbo && turboAvailable { // Turbo time
			// log.Printf("Turbo activated")
			turboAvailable = false
			send_turbo(writer)
		} else if sideSwitch != "None" && pendingSwitchCommand != sideSwitch && newThrottle == lastThrottle { // Switch lane
			// Switch to shortest lane
			// log.Printf("Switch to %s", sideSwitch)
			pendingSwitchCommand = sideSwitch
			send_switch(writer, sideSwitch)
		} else { // Adjust throttle
			// Set throttle
			// log.Printf("Set throttle to %v", newThrottle)
			lastThrottle = newThrottle
			send_throttle(writer, newThrottle)
		}
	}()

	if !gameStarted || !initDone {
		newThrottle = 1.0
		return
	}

	// Default useTurbo to false
	useTurbo = false

	// Parse data
	mdata, ok := data.([]interface{})
	if !ok {
		// log.Printf("Could not cast car positions data")
		newThrottle = defaultThrottle
		return
	}

	for _, md := range mdata {
		// Put in a struct (marshal then unmarshal, bad hack)
		jdata, err := json.Marshal(md)
		if err != nil {
			// log.Printf("Could not marshal carPosition(%d) data: %s", cid, err)
			continue
		}

		var carPosition CarPosition
		err = json.Unmarshal(jdata, &carPosition)
		if err != nil {
			// log.Printf("Could not unmarshal carPosition(%d) data: %s", cid, err)
			continue
		}
		defer func() {
			lastCarPosition = carPosition
		}()

		if carPosition.Id.Name == "GoExhaust" {
			var wg sync.WaitGroup
			currentPieceIndex := carPosition.PiecePosition.PieceIndex
			nextPieceIndex := gameInit.Race.Track.Pieces[currentPieceIndex].NextPieceIndex
			nextCurveIndex := gameInit.Race.Track.Pieces[currentPieceIndex].NextCurveIndex
			nextSwitchIndex := gameInit.Race.Track.Pieces[currentPieceIndex].NextSwitchIndex
			nextSwitchAfterNextSwitch := gameInit.Race.Track.Pieces[nextSwitchIndex].NextSwitchIndex
			switchedPiece := lastCarPosition.PiecePosition.PieceIndex != currentPieceIndex
			currentLane := carPosition.PiecePosition.Lane.StartLaneIndex
			defer func() {
				wg.Wait()
				lastLane = currentLane
			}()

			// Switch lane command was processed, reset
			if lastLane != currentLane {
				pendingSwitchCommand = "None"
			}

			// Move towards best lane
			wg.Add(1)
			go func() {
				defer wg.Done()
				shortestLane := currentLane
				shortestLaneDist := 0.0
				for sid := nextSwitchIndex; sid < nextSwitchAfterNextSwitch; sid++ {
					shortestLaneDist += gameInit.Race.Track.Pieces[sid].LaneLengths[currentLane]
				}

				for b := 0; b < len(gameInit.Race.Track.Lanes); b++ {
					if b == currentLane {
						continue
					}
					laneDist := 0.0
					for sid := nextSwitchIndex; sid < nextSwitchAfterNextSwitch; sid++ {
						laneDist += gameInit.Race.Track.Pieces[sid].LaneLengths[b]
					}
					if laneDist < shortestLaneDist {
						shortestLane = b
						shortestLaneDist = laneDist
					}
				}
				if shortestLane < currentLane {
					sideSwitch = "Left"
				} else if shortestLane > currentLane {
					sideSwitch = "Right"
				} else {
					sideSwitch = "None"
				}
			}()

			wg.Add(1)
			go func() {
				defer wg.Done()

				// Measure how much distance was travelled since last tick (speed)
				if switchedPiece {
					lastPieceLaneLenght := gameInit.Race.Track.Pieces[lastCarPosition.PiecePosition.PieceIndex].LaneLengths[lastCarPosition.PiecePosition.Lane.StartLaneIndex]
					speed = math.Abs(lastPieceLaneLenght-lastCarPosition.PiecePosition.InPieceDistance) + carPosition.PiecePosition.InPieceDistance
				} else {
					speed = carPosition.PiecePosition.InPieceDistance - lastCarPosition.PiecePosition.InPieceDistance
				}
				defer func() {
					lastSpeed = speed
				}()

				// In a curve
				if gameInit.Race.Track.Pieces[currentPieceIndex].Length == nil {
					defer func() {
						if newThrottle == 0.0 {
							inCurveMaxSlowDownCount++
						}
					}()

					// Get some info about draft
					draftChange := carPosition.Angle - lastCarPosition.Angle
					adjustedDraftChange := draftChange
					if *gameInit.Race.Track.Pieces[currentPieceIndex].Angle < 0.0 { // left turn, angles are negative values
						adjustedDraftChange *= -1
					}

					// Measure current lane radius
					currentPieceLaneRadius := *gameInit.Race.Track.Pieces[currentPieceIndex].Radius
					if *gameInit.Race.Track.Pieces[currentPieceIndex].Angle < 0 {
						currentPieceLaneRadius += gameInit.Race.Track.Lanes[currentLane].DistanceFromCenter
					} else {
						currentPieceLaneRadius -= gameInit.Race.Track.Lanes[currentLane].DistanceFromCenter
					}

					// If we switched piece, save entry speed for adjustments
					if switchedPiece {
						currentCurvePieceEntrySpeed = speed
					}

					// Slow down first few ticks of a curve to see draft changes
					inCurveTicksCount++
					if inCurveTicksCount < minTicksInCurveBeforeAccelerate && *gameInit.Race.Track.Pieces[currentPieceIndex].Radius < 200 {
						// log.Printf("(%d)(%v) Init curve slow down (%v vs %v)", currentPieceIndex, speed, lastCarPosition.Angle, carPosition.Angle)
						newThrottle = 0.0
						return
					}

					// If we slow down X times in a row, push the throttle to see if car's draft will get lower
					if inCurveMaxSlowDownCount > inCurveMaxSlowDown {
						// log.Printf("(%d)(%v) In curve try to lower draft with gas (%v vs %v)", currentPieceIndex, speed, lastCarPosition.Angle, carPosition.Angle)
						newThrottle = 1.0
						inCurveMaxSlowDownCount = 0
						lastTriedStraighten = true
						lastTriedStraightenDraftChange = adjustedDraftChange
						return
					}

					// If last command was a gas to trying to straighten car, if it did straighten keep pressing gas
					if lastTriedStraighten && adjustedDraftChange < lastTriedStraightenDraftChange {
						// log.Printf("(%d)(%v) In curve keep trying to lower draft with gas (%v vs %v)", currentPieceIndex, speed, lastCarPosition.Angle, carPosition.Angle)
						newThrottle = 1.0
						inCurveMaxSlowDownCount = 0
						lastTriedStraighten = true
						lastTriedStraightenDraftChange = adjustedDraftChange
						return
					}
					lastTriedStraighten = false

					// Finished first X entry ticks, check draft vs entry speed and propagate adjustments
					if carPosition.PiecePosition.Lap == 0 && inCurveMaxSlowDownCount == inCurveMaxSlowDown {
						speedToSave := currentCurvePieceEntrySpeed
						if adjustedDraftChange > maxEntryDraftChange {
							// Save an entry speed slightly lower than previous entry speed
							speedToSave *= 0.80
						}

						for pid, piece := range gameInit.Race.Track.Pieces {
							if pid <= currentPieceIndex {
								continue
							}
							// If it's a curve
							if piece.Length == nil {
								// Check each lane
								for lid, lane := range gameInit.Race.Track.Lanes {
									laneRadius := 0.0
									if piece.Angle != nil && *piece.Angle < 0.0 { // curves to the left, inside lane is to the left of center
										laneRadius = *piece.Radius + lane.DistanceFromCenter
									} else { // curves to the right, inside lane is to the right of center
										laneRadius = *piece.Radius + (-1.0 * lane.DistanceFromCenter)
									}
									// Check if radius is equal or greater
									speedDiff := speedToSave - gameInit.Race.Track.Pieces[pid].MaxSpeed[lid]
									if laneRadius <= currentPieceLaneRadius && speedDiff > 0.0 {
										gameInit.Race.Track.Pieces[pid].MaxSpeed[lid] = gameInit.Race.Track.Pieces[pid].MaxSpeed[lid] + (0.9 * speedDiff)
									}
								}
							}
						}
					}

					// If going into a straight piece and draft angle changes over distance remaining is still under max draft, speed up (end of curve acceleration)
					if gameInit.Race.Track.Pieces[nextPieceIndex].Length != nil {
						curveDistRemaining := gameInit.Race.Track.Pieces[currentPieceIndex].LaneLengths[currentLane] - carPosition.PiecePosition.InPieceDistance
						estimatedEndAngle := math.Abs((curveDistRemaining/speed)*adjustedDraftChange) + math.Abs(carPosition.Angle)
						if estimatedEndAngle < maxExitDraft {
							// Use turbo in last lap best spot
							if carPosition.PiecePosition.Lap == gameInit.Race.RaceSession.Laps-1 && nextPieceIndex == longuestStraightStartIndex && qualifications {
								useTurbo = true
							}
							inCurveMaxSlowDownCount = 0
							newThrottle = 1.0
							return
						}
					}

					// If need to slow down for next curve, slow down...

					// If stable accelerate
					if adjustedDraftChange < stableDraft {
						if gameInit.Race.Track.Pieces[currentPieceIndex].MaxSpeed[currentLane] < currentCurvePieceEntrySpeed {
							gameInit.Race.Track.Pieces[currentPieceIndex].MaxSpeed[currentLane] = currentCurvePieceEntrySpeed
						}
						inCurveMaxSlowDownCount = 0
						newThrottle = 1.0
						return
					}

					// If draft angle changes too fast or draft is increasing and already over the limit, slow down
					currentPieceMaxDraftAcceleration := 0.0
					currentPieceMaxDraft := 0.0
					if *gameInit.Race.Track.Pieces[currentPieceIndex].Radius < 100 {
						currentPieceMaxDraftAcceleration = maxDraftAccelerationUnder100
						currentPieceMaxDraft = maxDraftUnder100
					} else if *gameInit.Race.Track.Pieces[currentPieceIndex].Radius < 200 {
						currentPieceMaxDraftAcceleration = maxDraftAccelerationUnder200
						currentPieceMaxDraft = maxDraftUnder200
					} else {
						currentPieceMaxDraftAcceleration = maxDraftAccelerationUnder300
						currentPieceMaxDraft = maxDraftUnder300
					}
					if adjustedDraftChange > currentPieceMaxDraftAcceleration || (adjustedDraftChange > 0.0 && math.Abs(carPosition.Angle) > currentPieceMaxDraft) {
						// log.Printf("(%d)(%v) In curve slow down (%v vs %v)", currentPieceIndex, speed, lastCarPosition.Angle, carPosition.Angle)
						newThrottle = 0.0
						return
					}

					// Else accelerate
					// log.Printf("(%d)(%v) In curve accelerate (%v vs %v)", currentPieceIndex, speed, lastCarPosition.Angle, carPosition.Angle)
					inCurveMaxSlowDownCount = 0
					newThrottle = 1.0
					return

					// if val, ok := gameInit.Race.Track.Pieces[currentPieceIndex].MaxDraft[currentLane]; !ok || math.Abs(carPosition.Angle) > val {
					// 	gameInit.Race.Track.Pieces[currentPieceIndex].MaxDraft[currentLane] = math.Abs(carPosition.Angle)
					// }

					return
				} else { // In a straight piece
					// Reset curve max slow down count and ticks count since in a straight line
					inCurveMaxSlowDownCount = 0
					inCurveTicksCount = 0

					// Use turbo in last lap best spot
					if carPosition.PiecePosition.Lap == gameInit.Race.RaceSession.Laps-1 && currentPieceIndex == longuestStraightStartIndex && qualifications {
						useTurbo = true
					}

					// If on last lap and curve is after end of race, go full blast
					if carPosition.PiecePosition.Lap == gameInit.Race.RaceSession.Laps-1 && nextCurveIndex < currentPieceIndex {
						useTurbo = true
						// log.Printf("(%d)(%v) Straight last lap accelerating", currentPieceIndex, speed)
						newThrottle = 1.0
						return
					}

					// Check if we can continue at full blast or we need to decelarate
					tmpDistanceToNextCurve := gameInit.Race.Track.Pieces[currentPieceIndex].DistanceToNextCurve + (gameInit.Race.Track.Pieces[currentPieceIndex].LaneLengths[carPosition.PiecePosition.Lane.StartLaneIndex] - carPosition.PiecePosition.InPieceDistance)

					tmpCurveEntrySpeed := defaultCurveEntrySpeedUnder100
					if entrySpeed, ok := gameInit.Race.Track.Pieces[nextCurveIndex].MaxSpeed[currentLane]; ok {
						tmpCurveEntrySpeed = entrySpeed
					}

					// If going slower than curve entry speed, accelerate
					if speed < tmpCurveEntrySpeed {
						// log.Printf("(%d)(%v) Straight accelerating", currentPieceIndex, speed)
						newThrottle = 1.0
						return
					}

					// If current draft is not 0, adjust slightly
					if math.Abs(carPosition.Angle) > 20.0 && *gameInit.Race.Track.Pieces[nextCurveIndex].Radius < 200 {
						tmpCurveEntrySpeed *= 0.8
					} else if math.Abs(carPosition.Angle) > 10.0 && *gameInit.Race.Track.Pieces[nextCurveIndex].Radius < 200 {
						tmpCurveEntrySpeed *= 0.9
					}

					distTravelledBeforeCurve := speed
					tmpEndSpeed := speed
				S:
					for {
						roundedSpeed := int(math.Floor(tmpEndSpeed + 0.5))
						tmpEndSpeed = tmpEndSpeed - decelerationRef[roundedSpeed]
						distTravelledBeforeCurve = distTravelledBeforeCurve + tmpEndSpeed
						if tmpEndSpeed <= tmpCurveEntrySpeed || distTravelledBeforeCurve >= tmpDistanceToNextCurve {
							break S
						}
					}
					if distTravelledBeforeCurve < tmpDistanceToNextCurve {
						// log.Printf("(%d)(%v) Straight accelerating (DistanceToNextCurve: %f, tmpEndSpeed: %f)", currentPieceIndex, speed, tmpDistanceToNextCurve, tmpEndSpeed)
						newThrottle = 1.0
						return
					} else {
						// log.Printf("(%d)(%v) Straight slowing down (DistanceToNextCurve: %f, tmpEndSpeed: %f)", currentPieceIndex, speed, tmpDistanceToNextCurve, tmpEndSpeed)
						newThrottle = 0.0
						return
					}
				}
			}()

		} else {
			// Track other bot's positions too
			continue
		}
	}

	// Default?? should not get down here
	newThrottle = defaultThrottle
	return
}

//go build hwo2014-team-1347\go\src\noobbot
