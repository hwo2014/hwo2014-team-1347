package main

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"net"
	"os"
	"strconv"
)

func read_msg(reader *bufio.Reader) (msg interface{}, err error) {
	var line string
	line, err = reader.ReadString('\n')
	if err != nil {
		return
	}
	err = json.Unmarshal([]byte(line), &msg)
	if err != nil {
		return
	}
	return
}

func write_msg(writer *bufio.Writer, msgtype string, data interface{}) (err error) {
	m := make(map[string]interface{})
	m["msgType"] = msgtype
	m["data"] = data
	var payload []byte
	payload, err = json.Marshal(m)
	_, err = writer.Write([]byte(payload))
	if err != nil {
		return
	}
	_, err = writer.WriteString("\n")
	if err != nil {
		return
	}
	writer.Flush()
	return
}

func parse_and_dispatch_input(writer *bufio.Writer, input interface{}) (err error) {
	switch t := input.(type) {
	default:
		err = errors.New(fmt.Sprintf("Invalid message type: %T", t))
		return
	case map[string]interface{}:
		var msg map[string]interface{}
		var ok bool
		msg, ok = input.(map[string]interface{})
		if !ok {
			err = errors.New(fmt.Sprintf("Invalid message type: %v", msg))
			return
		}
		switch msg["data"].(type) {
		default:
			err = dispatch_msg(writer, msg["msgType"].(string), nil)
			if err != nil {
				return
			}
		case interface{}:
			err = dispatch_msg(writer, msg["msgType"].(string), msg["data"].(interface{}))
			if err != nil {
				return
			}
		}
	}
	return
}

func parse_args() (host string, port int, name string, key string, err error) {
	args := os.Args[1:]
	if len(args) != 4 {
		return "", 0, "", "", errors.New("Usage: ./run host port botname botkey")
	}
	host = args[0]
	port, err = strconv.Atoi(args[1])
	if err != nil {
		return "", 0, "", "", errors.New(fmt.Sprintf("Could not parse port value to integer: %v\n", args[1]))
	}
	name = args[2]
	key = args[3]

	return
}

// Main bot loop
func bot_loop(conn net.Conn, name string, key string) (err error) {
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	// Join
	send_join(writer, name, key)
	// send_join_race(writer, name, key, "keimola", 1)
	// send_join_race(writer, name, key, "usa", 1)
	// send_join_race(writer, name, key, "germany", 1)

	// Race
	for {
		input, err := read_msg(reader)
		if err != nil {
			return fmt.Errorf("Read msg error: %s", err)
		}
		err = parse_and_dispatch_input(writer, input)
		if err != nil {
			return fmt.Errorf("Parse and dispatch input error: %s", err)
		}
	}
	return nil
}

func send_join(writer *bufio.Writer, name string, key string) (err error) {
	data := make(map[string]string)
	data["name"] = name
	data["key"] = key
	err = write_msg(writer, "join", data)
	return
}

func send_ping(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "ping", make(map[string]string))
	return
}

func send_throttle(writer *bufio.Writer, throttle float32) (err error) {
	err = write_msg(writer, "throttle", throttle)
	return
}

func send_switch(writer *bufio.Writer, side string) (err error) {
	err = write_msg(writer, "switchLane", side)
	return
}

func send_turbo(writer *bufio.Writer) (err error) {
	err = write_msg(writer, "turbo", "YEEeeEEeeEEeeEEeeEEee")
	return
}

func send_join_race(writer *bufio.Writer, name string, key string, trackName string, carCount int) (err error) {
	botId := make(map[string]string)
	botId["name"] = name
	botId["key"] = key
	data := make(map[string]interface{})
	data["botId"] = botId
	data["trackName"] = trackName
	data["carCount"] = carCount
	err = write_msg(writer, "joinRace", data)
	return

}
