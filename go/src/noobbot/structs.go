package main

// --------------------------------
// GameInit structs
type GameInit struct {
	Race Race `json:"race"`
}

type Race struct {
	Track       Track       `json:"track"`
	Cars        []Car       `json:"cars"`
	RaceSession RaceSession `json:"raceSession"`
}

type Track struct {
	Id            string        `json:"id"`
	Name          string        `json:"name"`
	Pieces        []Piece       `json:"pieces"`
	Lanes         []Lane        `json:"lanes"`
	StartingPoint StartingPoint `json:"startingPoint"`
}

type Piece struct {
	Length              *float64        `json:"length"`
	Switch              *bool           `json:"switch"`
	Radius              *float64        `json:"radius"`
	Angle               *float64        `json:"angle"`
	LaneLengths         map[int]float64 // Per lane lenght (inside lane is shorter...)
	NextPieceIndex      int             // Next piece index
	DistanceToNextCurve float64         // Distance to next curve
	NextCurveIndex      int             // Next piece index where it's a curve
	NextSwitchIndex     int             // Next piece index where a lane swith is possible
	MaxSpeed            map[int]float64 // For curve, max speed in piece lane without crashing
	// MaxDraft            map[int]float64 // For curve, max draft in piece lane without crashing
}

type Lane struct {
	DistanceFromCenter float64 `json:"distanceFromCenter"`
	Index              int     `json:"index"`
}

type StartingPoint struct {
	Position Position `json:"position"`
	Angle    float64  `json:"angle"`
}

type Position struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
}

type Car struct {
	Id         CarId        `json:"id"`
	Dimensions CarDimension `json:"dimensions"`
}

type CarId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type CarDimension struct {
	Length float64 `json:"length"`
	Width  float64 `json:"width"`
	Guide  float64 `json:"guideFlagPosition"`
}

type RaceSession struct {
	Laps         int  `json:"laps"`
	MaxLapTimeMs int  `json:"maxLapTimeMs"`
	QuickRace    bool `json:"quickRace"`
}

// --------------------------------
// Car position structs
type CarPosition struct {
	Id            CarPositionId `json:"id"`
	Angle         float64       `json:"angle"`
	PiecePosition PiecePosition `json:"piecePosition"`
}

type CarPositionId struct {
	Name  string `json:"name"`
	Color string `json:"color"`
}

type PiecePosition struct {
	PieceIndex      int               `json:"pieceIndex"`
	InPieceDistance float64           `json:"inPieceDistance"`
	Lane            PiecePositionLane `json:"lane"`
	Lap             int               `json:"lap"`
}

type PiecePositionLane struct {
	StartLaneIndex int `json:"startLaneIndex"`
	EndLaneIndex   int `json:"endLaneIndex"`
}
